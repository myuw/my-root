# Root context and error valve change log

## 1.3.2 - 2024-02-26

+ Normalize on font-weight 630 for headings
+ Adjust font-size and line-height

## 1.3.1

(This version partially published due to a transient artifactorydoit error.)

## 1.3.0 - 2024-01-17

+ Link to Applications and Resources KB page
+ Use Red Hat fonts
+ Add text attributing the 500 error page.

## 1.2.1 - 2022-03-14

+ Update root 500 service error page. Refreshes link labels and URLs and organization of page.

## 1.2.0 - 2021-10-22

+ Label the root 403 error page with error code MYUW-002.
+ Label the root 404 error page with error code MYUW-003.
+ Use root-relative link to feedback so that feedback remains in correct hostname
  (so, in my.wisc.edu the feedback link will be to my.wisc.edu,
  but in my.wisconsin.edu the feedback link will be to my.wisconsin.edu)

## 1.1.15 and earlier

Prior versions exist, but they predate this change log.
