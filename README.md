# MyUW root webapp and Tomcat error valve

There are two modules here: a webapp to serve as the root web application
context and plugin to customize Tomcat's error handling.

## Maven repository usage

✅ Publishes artifacts to artifactorydoit.jfrog.io.

## MyUW root webapp

This is a small webapp to serve as the root, default servlet application in MyUW
Tomcat. It contains the error pages to fall back upon when another servlet
context does not handle the request.

### Development of the root webapp

From the root directory, run `npm start`. The web app will run at
<http://localhost:8080> and will show the 404 page by
default.

## MyUW Tomcat error valve

Customizes Tomcat to use the error pages in the custom root webapp.

## Releasing

The root webapp and the valve are versioned and released together.

Release using the [myuw-root-RELEASE][] Jenkins job.

This will publish a release for whatever snapshot version the pom.xml indicates.
(So, if the root pom.xml indicates 1.4.2-SNAPSHOT, the Jenkins job will release 1.4.2).

After releasing, manually update the `pom.xml` files (plural) to build the new SNAPSHOT version.
(So, if you release 1.4.2, update to now build 1.4.3-SNAPSHOT or 1.5.0-SNAPSHOT or 2.0.0-SNAPASHOT depending upon what kinds of changes you expect to make next.)

When you release a new version of this project, you'll publish both a root
webapp .war and a Tomcat error valve .jar.

For these to be actually used in MyUW, update the tier-specific ears to
[reference the new root webapp version][MyUW ear use of root webapp],
and update the Tomcat container to
[use the new valve version][MyUW Tomcat use of valve].


[myuw-root-RELEASE]: https://tools.my.wisc.edu/jenkins/job/myuw-root-RELEASE
[MyUW ear use of root webapp]: https://git.doit.wisc.edu/myuw-overlay/myuw-ear/-/blob/qa/pom.xml#L54
[MyUW Tomcat use of valve]: https://git.doit.wisc.edu/myuw/tomcat7-java8/-/blob/master/Dockerfile#L8

